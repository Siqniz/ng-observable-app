import { Component, OnInit } from '@angular/core';

import { PostService } from '../post.service';
import { PostModel } from '../post.model';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  posts: PostModel[] = [];// new MatTableDataSource<PostModel>();
  //displayedColumns: string[] = ['Title', 'Body']
  /**
   *
   */
  constructor(private svc: PostService) { }

  ngOnInit(): void {
    this.svc.getPosts().subscribe(data => {
      this.posts = data
    });
  }


  title = 'app';
}
