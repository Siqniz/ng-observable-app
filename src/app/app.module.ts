import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

//Material
import {MatTableModule} from '@angular/material'

import {PostService} from '../post.service'
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
    

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
