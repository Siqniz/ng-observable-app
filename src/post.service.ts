import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';

import { map } from 'rxjs/operators';
import { PostModel } from './post.model';
import { Injectable } from '@angular/core';

@Injectable()
export class PostService {
    posts: PostModel[] = []
    /**
     *
     */
    constructor(private client: HttpClient) {

    }
    getPosts(): Observable<PostModel[]> {
        return this.client.get<PostModel[]>('https://jsonplaceholder.typicode.com/posts')
            .pipe(map(res => {
                return this.posts = res //If needed I can do more mapping xforms from here
            }))
    };
}